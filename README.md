# Tweet sentiment analysis 

## Требования
* Python 3.6


#### Установка проекта:
Скачайте репозиторий и перейдите в папку проекта
```bash
git clone 
cd tweet_sentiment
```

Создайте виртуальную среду
```bash
virtualenv --python python3 --prompt "(tweet) " venv
```

Активируйте виртуальную среду
```bash
. venv/bin/activate
```

Установите зависимости
```bash
pip install -r requirements.txt
```
Загрузите архив Documents.zip с файлами
three_minutes_tweets.json.txt
AFINN-111.txt в директорию
```bash
tweet_sentiment/files/downloads
```

#### Команды

Инициализация базы данных
```bash
python main.py initdb
```

Добавить колонку tweet_sentimental в таблицу base_tweets
```bash
python main.py addcolumnn
```

Наполнить бд данными из three_minutes_tweets.json.txt
```bash
python main.py loaddata
```

Нормализовать таблицу base_tweets
```bash
python main.py normalize
```

Подсчитать средний sentiment сообщений
```bash
python main.py tweets_sentiment_values
```

Вывести наиболее и наименее счастливую страну, локацию и пользователя
```bash
python main.py happiest_unhappiest_values
```