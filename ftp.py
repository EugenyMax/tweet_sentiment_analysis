from ftplib import FTP
from time import sleep
from config import DOWNLOADS_DIR
from load_data import load_data


def monitor(server, directory=DOWNLOADS_DIR, interval=50):

    ftp = FTP(server)
    ftp.login()
    ftp.cwd(directory)

    while True:
        new_files = ftp.nlst()
        if new_files:
            load_data()
        sleep(interval)
    ftp.quit()

monitor('ftp.server')
