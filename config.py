import os
import logging

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
FILES_DIR = os.path.join(BASE_DIR, 'files')
DOWNLOADS_DIR = os.path.join(FILES_DIR, 'downloads')
UNLOADS_DIR = os.path.join(FILES_DIR, 'unloads')


# logging settings
def get_logger():
    logger = logging.getLogger('TWEET LOGGER')
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)s - %(asctime)s - %(message)s')
    path = os.path.join(FILES_DIR, 'tweet_sentiment.log')
    fh = logging.FileHandler(path)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    # comment 4 lines under this if you don't want view logs on command line
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    # logger.addHandler(ch)
    return logger


tweet_logger = get_logger()

