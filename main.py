import click

from db.commands import init_db, upgrade_tweet, normalize_table, \
    get_tweet_sentiment_values, get_happiest_unhappiest_values
from load_data import load_data


@click.group()
def main():
    pass


@main.command('initdb', short_help='Create tables')
def initdb():
    init_db()
    click.echo('Initialized the database')


@main.command('addcolumnn', short_help='Add columnt `tweet_sentiment` to table `tweets`')
def addcolumnn():
    upgrade_tweet()
    click.echo('Column added')


@main.command('loaddata', short_help='Load data from file to database')
def loaddata():
    load_data()
    click.echo('Loaded json data to database')


@main.command('normalize', short_help='Table `tweets` normalization')
def normalize():
    normalize_table()
    click.echo('Table normalized')


@main.command('tweets_sentiment_values', short_help='Get tweets sentiment values')
def tweets_sentiment_values():
    get_tweet_sentiment_values()
    click.echo('tweets sentiment values updated')\


@main.command('happiest_unhappiest_values', short_help='Get happiest unhappiest values')
def happiest_unhappiest_values():
    get_happiest_unhappiest_values()


@main.command('rundb', short_help='run all init commands')
def rundb():
    init_db()
    load_data()
    upgrade_tweet()
    normalize_table()
    get_tweet_sentiment_values()
    click.echo('Successfull finish rundb!')


if __name__ == '__main__':
    main()
