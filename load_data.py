import json
import os
import datetime
import zipfile
from config import DOWNLOADS_DIR, tweet_logger
from db import TweetBase, get_session

session = get_session()


def unzip_data(file_name='Documents_.zip'):
    path = os.path.join(DOWNLOADS_DIR, file_name)
    try:
        zip_ref = zipfile.ZipFile(path, 'r')
        zip_ref.extractall(DOWNLOADS_DIR)
        zip_ref.close()
    except FileNotFoundError as e:
        tweet_logger.error(e)


def load_data():
    unzip_data()
    tweet_logger.info('Parsing data and loading to db')
    path = os.path.join(DOWNLOADS_DIR, 'three_minutes_tweets.json.txt')
    with open(path) as f:
        for i, line in enumerate(f):
            data = json.loads(line)
            try:
                new_tweet = TweetBase(
                    name=data['user']['screen_name'],
                    tweet_text=data['text'],
                    display_url='https://twitter.com/{}/status/{}'.format(data['user']['screen_name'], data['id']),
                    lang=data['user']['lang'],
                    created_at=datetime.datetime.strptime(data['created_at'], '%a %b %d %H:%M:%S %z %Y'),
                    location=data['user']['location'],
                )
                if data['place']:
                    new_tweet.country_code = data['place']['country_code']
                session.add(new_tweet)
            except KeyError as e:
                tweet_logger.error(data)
    session.commit()
