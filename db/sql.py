from sqlalchemy import text

colums = {
    'id': 'id',
    'name': 'name',
    'tweet_text': 'tweet_text',
    'country_code': 'country_code',
    'display_url': 'display_url',
    'lang': 'lang',
    'created_at': 'created_at',
    'location': 'location',
    'tweet_sentiment': 'tweet_sentiment',
    'user_id': 'user_id',
    'lang_id': 'lang_id',
    'country_code_id': 'country_code_id',
    'location_id': 'location_id',
}
users_table = 'users'
langs_table = 'langs'
locations_table = 'locations'
country_codes_table = 'country_codes'
tweets_table = 'base_tweets'
normalized_tweets_table = 'tweets'

# creating new tables

CREATE_USERS_TABLE_SQL = text("""
    CREATE TABLE IF NOT EXISTS {} (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL);""".format(users_table))

CREATE_LANGS_TABLE_SQL = text("""
    CREATE TABLE IF NOT EXISTS {} (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    short_name VARCHAR(15) NOT NULL);""".format(langs_table))

CREATE_COUNTRY_CODES_TABLE_SQL = text("""
    CREATE TABLE IF NOT EXISTS {} (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    short_name VARCHAR(15));""".format(country_codes_table))

CREATE_LOCATIONS_TABLE_SQL = text("""
    CREATE TABLE IF NOT EXISTS {} (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    name VARCHAR);""".format(locations_table))

# creating new normalized tweets table
CREATE_NEW_TABLE_TWEETS_NORM_SQL = text("""
    CREATE TABLE "{normalized_tweets_table}" (
    "{id}" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "{tweet_text}" varchar,
    "{display_url}" varchar,
    "{created_at}" datetime,
    "{tweet_sentiment}" integer,
    "{user_id}" integer NOT NULL REFERENCES "{users_table}" ("id") DEFERRABLE INITIALLY DEFERRED,
    "{lang_id}" integer NOT NULL REFERENCES "{langs_table}" ("id") DEFERRABLE INITIALLY DEFERRED,
    "{country_code_id}" integer REFERENCES "{country_codes_table}" ("id") DEFERRABLE INITIALLY DEFERRED,
    "{location_id}" integer REFERENCES "{locations_table}" ("id") DEFERRABLE INITIALLY DEFERRED);
    """.format(users_table=users_table, tweets_table=tweets_table,
               langs_table=langs_table, country_codes_table=country_codes_table,
               normalized_tweets_table=normalized_tweets_table,
               locations_table=locations_table, **colums))

# transfer data from non-normalized tweets table to tables-dictionaries
TRANSFER_USERS_DATA_SQL = text("""
    INSERT INTO "{users_table}" ("{name}")
    SELECT DISTINCT "{name}" FROM "{tweets_table}";""".format(
    users_table=users_table, tweets_table=tweets_table, **colums))

TRANSFER_LANGS_DATA_SQL = text("""
    INSERT INTO "{langs_table}" ("short_name")
    SELECT DISTINCT "{lang}" FROM "{tweets_table}";""".format(
    tweets_table=tweets_table, langs_table=langs_table, **colums))

TRANSFER_COUNTRY_CODES_DATA_SQL = text("""
    INSERT INTO "{country_codes_table}" ("short_name")
    SELECT DISTINCT "{country_code}" FROM "{tweets_table}"
    WHERE "{tweets_table}"."{country_code}" > \'\';""".format(
    tweets_table=tweets_table, country_codes_table=country_codes_table,
    **colums))

TRANSFER_LOCATIONS_DATA_SQL= text("""
    INSERT INTO "{locations_table}" ("name")
    SELECT DISTINCT "{location}" FROM "{tweets_table}"
    WHERE "{tweets_table}"."{location}" > \'\';""".format(
    tweets_table=tweets_table, locations_table=locations_table,
    **colums))

# transfer data from non-normalized tweets table to normalized
TRANSFER_TWEETS_DATA_SQL = text("""
    INSERT INTO "{normalized_tweets_table}" ("{id}", "{tweet_text}",
    "{display_url}", "{created_at}", "{tweet_sentiment}", "{user_id}",
    "{lang_id}", "{country_code_id}", "{location_id}")
    SELECT "{id}", "{tweet_text}",
    "{display_url}", "{created_at}", "{tweet_sentiment}",
    (SELECT id FROM {users_table} WHERE {users_table}.name = {tweets_table}.name),
    (SELECT id FROM {langs_table} WHERE {langs_table}.short_name = {tweets_table}.lang),
    (SELECT id FROM {country_codes_table} WHERE {country_codes_table}.short_name = {tweets_table}.country_code),
    (SELECT id FROM {locations_table} WHERE {locations_table}.name = {tweets_table}.location)
    FROM "{tweets_table}";""".format(
    users_table=users_table, tweets_table=tweets_table,
    langs_table=langs_table, country_codes_table=country_codes_table,
    normalized_tweets_table=normalized_tweets_table,
    locations_table=locations_table, **colums))

# drop non-normalized tweets table
DROP_BASE_TWEETS_TABLE = text("DROP TABLE \"{}\";".format(tweets_table))

# create indexes to ForeignKey fields
CREATE_USERS_INDEX_SQL = text(
    "CREATE INDEX \"{normalized_tweets_table}_user_id_tweet\" ON "
    "\"{normalized_tweets_table}\" (\"{user_id}\");".format(
        normalized_tweets_table=normalized_tweets_table,
        user_id=colums['user_id']))

CREATE_LANGS_INDEX_SQL = text(
    "CREATE INDEX \"{normalized_tweets_table}_lang_id_tweet\" ON "
    "\"{normalized_tweets_table}\" (\"{lang_id}\");".format(
        normalized_tweets_table=normalized_tweets_table,
        lang_id=colums['lang_id']))

CREATE_COUNTRY_CODES_INDEX_SQL = text(
    "CREATE INDEX \"{normalized_tweets_table}_country_code_id_tweet\" ON "
    "\"{normalized_tweets_table}\" (\"{country_code_id}\");".format(
        normalized_tweets_table=normalized_tweets_table,
        country_code_id=colums['country_code_id']))

CREATE_LOCATIONS_INDEX_SQL = text(
    "CREATE INDEX \"{normalized_tweets_table}_location_id_tweet\" ON "
    "\"{normalized_tweets_table}\" (\"{location_id}\");".format(
        normalized_tweets_table=normalized_tweets_table,
        location_id=colums['location_id']))

# getting happiest and unhappiest user

USER_HAPPIEST_QUERY_SQL = """WITH q AS (
        SELECT
          u.id,
          u.name,
          SUM(t.tweet_sentiment) sents
        FROM tweets t
          INNER JOIN users u ON t.user_id = u.id
        GROUP BY u.id
    ),
        unhappiest AS (SELECT * FROM q ORDER BY sents limit 1),
        happiest AS (SELECT * FROM q ORDER BY sents DESC limit 1)
    
    SELECT * FROM unhappiest
    union
    SELECT * FROM happiest ORDER BY sents DESC"""

# getting happiest and unhappiest country

COUNTRY_CODE_HAPPIEST_QUERY_SQL = """WITH q AS (
        SELECT
          cc.id,
          cc.short_name,
          SUM(t.tweet_sentiment) sents
        FROM tweets t
          INNER JOIN country_codes cc ON t.country_code_id = cc.id
        GROUP BY cc.id
    ),
        unhappiest AS (SELECT * FROM q ORDER BY sents limit 1),
        happiest AS (SELECT * FROM q ORDER BY sents DESC limit 1)
    
    SELECT * FROM unhappiest
    union
    SELECT * FROM happiest ORDER BY sents DESC"""

# getting happiest and unhappiest location

LOCATION_HAPPIEST_QUERY_SQL = """WITH q AS (
        SELECT
          loc.id,
          loc.name,
          SUM(t.tweet_sentiment) sents
        FROM tweets t
          INNER JOIN locations loc ON t.location_id = loc.id
        GROUP BY loc.id
    ),
        unhappiest AS (SELECT * FROM q ORDER BY sents limit 1),
        happiest AS (SELECT * FROM q ORDER BY sents DESC limit 1)
    
    SELECT * FROM unhappiest
    union
    SELECT * FROM happiest ORDER BY sents DESC"""