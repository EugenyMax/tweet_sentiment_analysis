from config import tweet_logger
from .models import TweetBase, Tweet
from pprint import pprint


def init_db():
    tweet_logger.info('Initializing the database')
    TweetBase._init_table()


def upgrade_tweet():
    tweet_logger.info('Adding sentiment column')
    TweetBase._add_text_sentiment()


def normalize_table():
    tweet_logger.info('Normalizing table')
    TweetBase._normalization()


def get_tweet_sentiment_values():
    tweet_logger.info('Getting tweets sentiment values')
    Tweet.count_tweets_sentiment()

def get_happiest_unhappiest_values():
    tweet_logger.info('Getting happiest unhappiest values')
    res = Tweet.get_happiest_and_unhappiest_values()
    tweet_logger.info(pprint(res))
