import os
from sqlalchemy import Column, DateTime, Integer, String, create_engine, text, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from afinn import Afinn
from config import BASE_DIR, DOWNLOADS_DIR, tweet_logger
from db.sql import *


Base = declarative_base()

engine = create_engine('sqlite:///{}/mts.db'.format(BASE_DIR))


def add_column(engine, table_name, column):
    column_name = column.compile(dialect=engine.dialect)
    column_type = column.type.compile(engine.dialect)
    engine.execute('ALTER TABLE %s ADD COLUMN %s %s' % (table_name, column_name, column_type))


def get_session():
    Session = sessionmaker(bind=engine)
    return Session()


class TweetBaseModel(Base):
    __abstract__ = True
    id = Column(Integer, primary_key=True)
    tweet_text = Column(String)
    display_url = Column(String)
    created_at = Column(DateTime)


class TweetBase(TweetBaseModel):
    __tablename__ = 'base_tweets'
    name = Column(String)
    lang = Column(String)
    country_code = Column(String)
    location = Column(String)

    def repr(self):
        return '<TweetBase(name="{}" text="{}" lang="{}")>'.format(
            self.name, self.tweet_text[:10], self.lang)

    @classmethod
    def exist_column(cls, col_name):
        sql = text('PRAGMA table_info({});'.format(cls.__tablename__))
        result = engine.execute(sql)
        names = [p[1] for p in result]
        return col_name in names

    @classmethod
    def _init_table(cls):
        cls.__table__.create(engine, checkfirst=True)

    @classmethod
    def _add_text_sentiment(cls):
        col_name = 'tweet_sentiment'
        if not cls.exist_column(col_name):
            column = Column('tweet_sentiment', Integer())
            add_column(engine, cls.__table__, column)

    @classmethod
    def _normalization(cls):

        # creating new tables
        engine.execute(CREATE_USERS_TABLE_SQL)
        engine.execute(CREATE_LANGS_TABLE_SQL)
        engine.execute(CREATE_COUNTRY_CODES_TABLE_SQL)
        engine.execute(CREATE_LOCATIONS_TABLE_SQL)

        # creating new normalized tweets table
        engine.execute(CREATE_NEW_TABLE_TWEETS_NORM_SQL)

        # transfer data from non-normalized tweets table to tables-dictionaries
        engine.execute(TRANSFER_USERS_DATA_SQL)
        engine.execute(TRANSFER_LANGS_DATA_SQL)
        engine.execute(TRANSFER_COUNTRY_CODES_DATA_SQL)
        engine.execute(TRANSFER_LOCATIONS_DATA_SQL)

        # transfer data from non-normalized tweets table to normalized
        engine.execute(TRANSFER_TWEETS_DATA_SQL)

        # drop non-normalized tweets table
        engine.execute(DROP_BASE_TWEETS_TABLE)

        # create indexes to ForeignKey fields
        engine.execute(CREATE_USERS_INDEX_SQL)
        engine.execute(CREATE_LANGS_INDEX_SQL)
        engine.execute(CREATE_COUNTRY_CODES_INDEX_SQL)
        engine.execute(CREATE_LOCATIONS_INDEX_SQL)


class Tweet(TweetBaseModel):
    """
    tweets table model after normalization
    """
    __tablename__ = 'tweets'
    tweet_sentiment = Column(Integer)
    user_id = Column('user_id', Integer, ForeignKey("users.id"), nullable=False)
    lang_id = Column('lang_id', Integer, ForeignKey("langs.id"), nullable=True)
    location_id = Column('location_id', Integer, ForeignKey("locations.id"), nullable=True)
    country_code_id = Column('country_code_id', Integer, ForeignKey("country_codes.id"), nullable=True)

    @classmethod
    def count_tweets_sentiment(cls):
        """
        Method for getting sentiment score for tweet message
        """
        session = get_session()
        tweets = session.query(cls).all()
        path = os.path.join(DOWNLOADS_DIR, 'AFINN-111.txt')
        afinn = Afinn()
        afinn.setup_from_file(path)
        for tweet in tweets:
            tweet.tweet_sentiment = afinn.score(tweet.tweet_text)
        session.commit()

    @classmethod
    def get_happiest_and_unhappiest_values(cls):
        """
        method getting values from location, user and country
        :return: dict
        """
        queries_dict = {
            'country': COUNTRY_CODE_HAPPIEST_QUERY_SQL,
            'user': USER_HAPPIEST_QUERY_SQL,
            'location': LOCATION_HAPPIEST_QUERY_SQL,
        }
        result_dict = {}
        for name,query in queries_dict.items():
            res = engine.execute(query)
            try:
                res = list(res)
                happiest = res[0][1]
                unhappiest = res[1][1]
            except (TypeError, ValueError, IndexError) as e:
                happiest = ""
                unhappiest = ""
                tweet_logger.error(e)
            result_dict[name] = {'happiest': happiest, 'unhappiest': unhappiest}

        return result_dict


class User(Base):
    """
    users table model
    """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return self.name


class Location(Base):
    """
    locations table model
    """
    __tablename__ = 'locations'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return self.name


class Lang(Base):
    """
    langs table model
    """
    __tablename__ = 'langs'
    id = Column(Integer, primary_key=True)
    short_name = Column(String(15))

    def __repr__(self):
        return self.short_name


class CountryCode(Base):
    """
    langs table model
    """
    __tablename__ = 'country_codes'
    id = Column(Integer, primary_key=True)
    short_name = Column(String(15))

    def __repr__(self):
        return self.short_name
